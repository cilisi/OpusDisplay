import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/page/Login'
import Main from '@/components/page/Main'
import Home from '@/components/page/Home'
import Register from '@/components/page/Register'
import SelfDisplay from '@/components/page/SelfDisplay'

Vue.use(Router)

export default new Router({
  routes: [{
    path:'/',
    component:Main,
    children:[{
        path:'/',
        component:Home
      },{
        path:'SelfDisplay',
        component:SelfDisplay
      }]
  },{
    path:'/Login',
    component:Login
  },{
    path:'/Register',
    component:Register
  },{
    path:'*',
    component:Main,
    children:[{
        path:'*',
        component:Home
      }]
  }],
  mode:'history'
})
